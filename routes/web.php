<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('polling', function () {
    return view('polling');
});

Route::get('/chat', function() {
    return view('home');
})->name('chat');

Route::get('file', function() {
    return view('file');
})->name('file');

Route::post('file', 'FileController@upload');
