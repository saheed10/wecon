<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>LetChat</title>

        <link href="css/custom.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="position-ref full-height col-md-6">
            <div class="content">
                <div class="title m-b-md">
                    Buddies
                </div>

                <div class="links form-group">
                    <a href="" >Anand</a>
                    <a href="" >Shashi</a>
                    <a href="" >Rupesh</a>
                    <a href="" >Rishi</a><br>
                    <textarea class="form-control" width="" height="" name="message-display"></textarea>
                    <br><input class="form-control" type="text" name="ip-msg">
                    <br><button type="button" id="send" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
<script src="/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
        var conn = new WebSocket('ws://localhost:8000');

        conn.onopen = function(e) {
            console.log("Connection established!");
        };

        conn.onmessage = function(e) {
            console.log(e.data);
        };
         
    $('#send').click(function(){
        console.log('clicked');
        conn.send(JSON.stringify({command: "subscribe", channel: "mychannel"}));    
    })
</script>

    </body>
</html>
