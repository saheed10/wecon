<?php

$socketResource = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_bind($socketResource, "localhost", "5959");
socket_listen($socketResource);

$clientSocketArray = array($socketResource);
 while (true) {
        // create a copy, so $clients doesn't get modified by socket_select()
        $read = $clientSocketArray;
        
        // get a list of all the clients that have data to be read from
        // if there are no clients with data, go to next iteration
        if (socket_select($read, $write = NULL, $except = NULL, 0) < 1)
            continue;
        
        // check if there is a client trying to connect
        if (in_array($sock, $read)) {
            // accept the client, and add him to the $clients array
            $clientSocketArray[] = $newsock = socket_accept($socketResource);
            
            // send the client a welcome message
            socket_write($newsock, "no noobs, but ill make an exception :)\n".
            "There are ".(count($clientSocketArray) - 1)." client(s) connected to the server\n");
            
            socket_getpeername($newsock, $ip);
            echo "New client connected: {$ip}\n";
            
            // remove the listening socket from the clients-with-data array
            $key = array_search($socketResource, $read);
            unset($read[$key]);
        }
    }
?>